
import asyncio
from os import listdir as _listdir
from os.path import join, splitext, basename, isdir
import sys

files = ["main.cpp", "src/"]
compiler_flags = ["-Wall", "-Iinclude"]
linker_flags = ["-lSDL2", "-lSDL2_image"]
build_dir = "build/"
cc = "/bin/g++"
output = "nuke"

def listdir(path):
    return [join(path, file) for file in _listdir(path)]

async def build_file(src):
    print("Compiling", src, "...")
    dst = join(build_dir, splitext(basename(src))[0]) + ".o"
    proc = await asyncio.create_subprocess_exec(cc, "-c", *compiler_flags, src, "-o", dst)
    return await proc.wait()

async def link_objects():
    print("Linking ...")
    proc = await asyncio.create_subprocess_exec(cc, "-o", output, *listdir(build_dir), *linker_flags)
    return await proc.wait()

input_files = []
for file in files:
    if isdir(file):
        input_files.extend(listdir(file))
    else:
        input_files.append(file)

async def main():
    stats = await asyncio.gather(*(build_file(file) for file in input_files))
    if any(stats):
        return 1
    return await link_objects()

sys.exit(asyncio.run(main()))
