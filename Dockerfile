FROM ubuntu:22.04 as build-nuke

WORKDIR /build

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y g++ libsdl2-dev libsdl2-image-dev && \
    mkdir /app

COPY imgs/ /app/imgs/
COPY include/ include/
COPY src/ src/
COPY main.cpp .
COPY ./utils/build.sh .
RUN ./build.sh
RUN cp -a /build/nuke /app/nuke


FROM ubuntu:22.04

WORKDIR /app

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y libsdl2-image-2.0 libsdl2-2.0

COPY --from=build-nuke /app/ /app/

CMD [ "/app/nuke" ]
