
#include <stdio.h>
#include <SDL2/SDL.h>

#include "include/animation.hpp"
#include "include/map.hpp"
#include "include/camera.hpp"
#include "include/images.hpp"
#include "include/player.hpp"
#include "include/world_gen.hpp"

#include <cmath>
#include <iostream>

#define WINDOW_WIDTH 500
#define WINDOW_HEIGHT 500

void dump_sdl_error(const char *message)
{
    fprintf(stderr, "%s: %s\n", message, SDL_GetError());
}

// block_t gen(blockpos_t x, blockpos_t y)
// {
//     int height = (std::sin(x / 2.0) + 1) * 3 + 15;

//     if (y < height)
//         return 0;
//     else if (y == height)
//         return 2;
//     return 1;
// }

int main()
{
    /* === variables === */
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Event event; // current event
    SDL_Rect addpos;
    SDL_Texture **blocks_images;

    bool mainloop;

    /* === init === */

    /* create window */
    if ((window = SDL_CreateWindow("Nuke", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, 0)) == NULL)
    {
        dump_sdl_error("Error while opening window");
        return 1;
    }

    /* create renderer */
    if ((renderer = SDL_CreateRenderer(window, -1, 0)) == NULL)
    {
        dump_sdl_error("Error while creating renderer");
        return 1;
    }

    blocks_images = load_blocks_images(renderer, "imgs/", 10);

    gen_function_t gen = genblock_earth;

    // Map game_map(22, gen, blocks_images);
    Map game_map(10, gen, blocks_images);

    Camera camera(50, WINDOW_WIDTH, WINDOW_HEIGHT);

    Player player(0, 0, &game_map);
    camera.set_follow_target(&player.rect);

    game_map.load_chunks(&player.rect);

    /* === mainloop === */
    mainloop = true;
    while (mainloop)
    {
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT: // window quit
                mainloop = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.scancode)
                {
                case SDL_SCANCODE_A:
                    if (player.auto_mv.addx == 0)
                        player.auto_mv.addx = -1;
                    break;
                case SDL_SCANCODE_D:
                    if (player.auto_mv.addx == 0)
                        player.auto_mv.addx = 1;
                    break;
                case SDL_SCANCODE_W:
                    player.jump(5);
                    break;
                default:
                    break;
                }
                break;
            case SDL_KEYUP:
                switch (event.key.keysym.scancode)
                {
                case SDL_SCANCODE_A:
                    if (player.auto_mv.addx == -1)
                        player.auto_mv.addx = 0;
                case SDL_SCANCODE_D:
                    if (player.auto_mv.addx == 1)
                        player.auto_mv.addx = 0;
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }

        SDL_SetRenderDrawColor(renderer, 0x20, 0x20, 0x20, 0xFF);
        SDL_RenderClear(renderer);

        camera.update();
        camera.get_addpos(&addpos);

        game_map.update();
        game_map.draw(renderer, &addpos);

        player.update();
        player.draw(renderer, &addpos);

        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }

    game_map.dealloc_all();

    return 0;
}
