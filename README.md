# Nuke 3.0 

## RUN INSIDE A CONTAINER

To run the game inside a container run:

```
docker run -it \
       --net=host \
       --env="DISPLAY" \
       --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
       registry.gitlab.com/lallo.segala/nuke-3.0
```
