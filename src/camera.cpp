
#include "camera.hpp"

Camera::Camera(float speed, int screen_width, int screen_height)
{
    addx = 0;
    addy = 0;
    zoom = 1.0;
    target = NULL;
    this->speed = speed;
    this->screen_width = screen_width;
    this->screen_height = screen_height;
}

int Camera::set_follow_target(SDL_Rect *target)
{
    SDL_Rect *old = this->target;
    this->target = target;
    return old != NULL;
}

void Camera::update()
{
    if (target == NULL)
        return;
    addx += (target->x - addx - (screen_width - target->w) / 2.0) / speed;
    addy += (target->y - addy - (screen_height - target->h) / 2.0) / speed;
}

void Camera::get_addpos(SDL_Rect *ptr)
{
    ptr->x = -addx;
    ptr->y = -addy;
}

int Camera::detach()
{
    if (target == NULL)
    {
        return 1;
    }
    target = NULL;
    return 0;
}

void Camera::shake(int force, int duration)
{
    // do
}

void Camera::set_screen_size(int w, int h)
{
    screen_width = w;
    screen_height = h;
}
