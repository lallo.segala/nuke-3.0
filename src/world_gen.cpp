
#include "world_gen.hpp"

#define DB_PERLIN_IMPL
#include "db_perlin.hpp"

block_t genblock_earth(blockpos_t x, blockpos_t y)
{
    /* from bottom to top */

    // bedrock
    if (y > CHUNK_HEIGHT - db::perlin(x / 3.0) * 3 - 2 || y == CHUNK_HEIGHT - 1)
        return 5;

    // black stone
    if (y > db::perlin(x / 10.0) * 5 + 50)
    {
        // green ore
        if ((db::perlin(x / 10.0, y / 10.0) + 1) / 2 < 0.25)
            return 9;
        return 4; // blackstone
    }

    //   rock level || hills || mountains
    if (y > db::perlin(x / 10.0) * 5 + 38 || y > db::perlin((x + 10) / 12.0) * 13 + 38 || y > db::perlin(x / 40.0) * 45 + 30)
    {
        if ((db::perlin(x / 10.0, y / 10.0) + 1) / 2 < 0.25)
            return 6; // coal
        if ((db::perlin(x / 5.0, y / 5.0) + 1) / 2 < 0.25)
            return 7; // iron
        if (y > 45 && (db::perlin((x + 20) / 5.0, (y + 10) / 5.0) + 1) / 2 < 0.25)
            return 8;
        return 3; // rock
    }

    int height = db::perlin(x / 10.0) * 10 + 30;
    if (y > height) // grass
        return 2;
    if (y == height) // dirt
        return 1;

    return 0; // air
}
