
#include <SDL2/SDL_timer.h>

#include "animation.hpp"

#include <iostream>

Animation::Animation()
{
    this->loop = true;
    this->stopped = false;
    this->finished = false;
    this->index = 0;
    this->last_update = 0;
    this->flipped = SDL_FLIP_NONE;

    this->num_frames = 0;
    this->frames = NULL;
}

Animation::~Animation()
{
    delete this->frames;
}

bool Animation::set_loop(bool loop)
{
    bool last = this->loop;
    this->loop = loop;
    return last;
}

bool Animation::pause()
{
    bool last = this->stopped;
    this->stopped = true;
    return last;
}

bool Animation::unpause()
{
    bool last = this->stopped;
    this->stopped = false;
    return last;
}

void Animation::finish()
{
    index = num_frames - 1;
    finished = true;
}

void Animation::draw(SDL_Renderer *renderer, SDL_Rect *rect)
{
    // SDL_RenderCopyEx(renderer, frames[index], NULL, rect, 0.0, NULL, flipped);
}

void Animation::update()
{
    if (last_update == 0)
    {
        last_update = SDL_GetTicks64();
        return;
    }

    if (stopped || SDL_GetTicks64() - last_update < delay)
    {
        return;
    }

    // update

    index++;
    last_update = SDL_GetTicks64();

    if (index < num_frames)
    {
        finished = false;
        return;
    }

    // finish

    finished = true;
    if (loop)
    {
        index = 0;
    }
    else
    {
        stopped = true;
    }
}
