
#include "logging.hpp"
#include <stdio.h>

void logs::info(const char *message)
{
    fprintf(stdout, "\033[1m[INFO]: %s\033[0m\n", message);
}

void logs::warn(const char *message)
{
    fprintf(stdout, "\033[1;93m[WARN]: %s\033[0m\n", message);
}

void logs::error(const char *message)
{
    fprintf(stdout, "\033[1;31m[ERROR]: %s\033[0m\n", message);
}
