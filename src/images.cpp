
#include <stdio.h>
#include <stdlib.h>

#include "images.hpp"
#include <SDL2/SDL_image.h>

SDL_Texture **load_blocks_images(SDL_Renderer *renderer, const char *path, int size)
{
    SDL_Texture **images = new SDL_Texture *[size];
    char *img_path;

    for (int i = 0; i < size; i++)
    {
        asprintf(&img_path, "%s/%d.png", path, i + 1);
        images[i] = IMG_LoadTexture(renderer, img_path);
        if (images[i] == NULL)
        {
            fprintf(stderr, "Unable to load image: %s\n", SDL_GetError());
            exit(1);
        }
        free(img_path);
    }

    return images;
}
