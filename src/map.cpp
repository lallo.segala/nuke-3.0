#include "map.hpp"
#include <iostream>

Chunk::Chunk(int chunkpos, gen_function_t gen_function)
{
    for (int y = 0; y < CHUNK_HEIGHT; y++)
    {
        for (int x = 0; x < CHUNK_WIDTH; x++)
        {
            blocks[y][x] = gen_function(x + chunkpos * CHUNK_WIDTH, y);
        }
    }
    this->chunkpos = chunkpos;
    printf("Chunk [%d]: allocated (%p)\n", chunkpos, this);
}

Chunk::~Chunk()
{
    printf("Chunk [%d]: deallocated (%p)\n", chunkpos, this);
}

void Chunk::update()
{
}

void Chunk::draw(SDL_Renderer *renderer, const SDL_Rect *addpos, SDL_Texture **blocks_textures)
{
    SDL_Rect block_rect = {0, 0, TILE_SIZE, TILE_SIZE};
    block_t block;

    int start_x = addpos->x + chunkpos * TILE_SIZE * CHUNK_WIDTH;
    int start_y = addpos->y;
    int stop_x = addpos->x + (chunkpos + 1) * TILE_SIZE * CHUNK_WIDTH - 1;
    int stop_y = addpos->y + TILE_SIZE * CHUNK_HEIGHT;

    for (int y = 0; y < CHUNK_HEIGHT; y++)
    {
        for (int x = 0; x < CHUNK_WIDTH; x++)
        {
            block = blocks[y][x];
            if (block == 0)
            {
                continue;
            }
            block_rect.x = x * TILE_SIZE + start_x;
            block_rect.y = y * TILE_SIZE + start_y;
            SDL_RenderCopy(renderer, blocks_textures[block - 1], NULL, &block_rect);
        }
    }

    // SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
    // SDL_RenderDrawLine(renderer, start_x, start_y, stop_x, start_y);
    // SDL_RenderDrawLine(renderer, start_x, stop_y, stop_x, stop_y);

    // SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0x00, 0xFF);
    // SDL_RenderDrawLine(renderer, start_x, start_y, start_x, stop_y);
    // SDL_RenderDrawLine(renderer, stop_x, start_y, stop_x, stop_y);

    // SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x00, 0xFF);
    // SDL_RenderDrawLine(renderer, start_x, start_y, stop_x, stop_y);

    // SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0xFF, 0xFF);
    // SDL_RenderDrawLine(renderer, stop_x, start_y, start_x, stop_y);
}

Map::Map(int render_distance, gen_function_t gen_function, SDL_Texture **blocks_textures)
{
    this->render_dist = render_distance;
    this->gen_function = gen_function;
    this->blocks_textures = blocks_textures;
}

void Map::load_chunks(SDL_Rect *rect)
{
    int chunkpos = rect->x / CHUNK_WIDTH;
    int center = render_dist / 2;

    dealloc_all();

    chunks_map.clear();

    for (int i = 0; i < render_dist; i++)
    {
        chunks_map[chunkpos + i - center] = new Chunk(chunkpos + i - center, gen_function);
    }
}

Chunk Map::load_chunk(int pos, gen_function_t gen_function)
{
    Chunk chunk(pos, gen_function);
    return chunk;
}

void Map::unpack_position(blockpos_t x, int *chunkpos, rel_blockpos_t *blockpos)
{
    *chunkpos = x / CHUNK_WIDTH;
    *blockpos = x % CHUNK_WIDTH;
}

int Map::setblock(blockpos_t x, blockpos_t y, block_t block)
{
    int chunkpos;
    rel_blockpos_t blockpos;

    if (y < 0 || y >= CHUNK_HEIGHT)
    {
        return BLOCK_IO_INVALID; // invalid pos
    }

    this->unpack_position(x, &chunkpos, &blockpos);

    setblock_ovr(chunkpos, blockpos, y, block);

    if (is_loaded(chunkpos))
    {
        chunks_map[chunkpos]->blocks[y][x] = block;
        return BLOCK_IO_OK; // ok
    }

    return BLOCK_IO_UNLOADED; // chunk not loaded
}

int Map::getblock(blockpos_t x, blockpos_t y, block_t *block)
{
    int chunkpos;
    rel_blockpos_t blockpos;

    if (y < 0 || y >= CHUNK_HEIGHT)
    {
        return BLOCK_IO_INVALID; // invalid pos
    }

    this->unpack_position(x, &chunkpos, &blockpos);

    if (is_loaded(chunkpos))
    {
        // do
        return BLOCK_IO_OK;
    }

    // if block not in ovr:
    //  - return error
    //  - gen block calling genblock
    //   ^^ may be a parameter: 'solve-block-not-found'

    // do
    return BLOCK_IO_UNLOADED;
}

void Map::setblock_ovr(int chunkpos, rel_blockpos_t x, rel_blockpos_t y, block_t block)
{
    // do
}

void Map::shift_chunks(int direction)
{
    int new_pos;

    if (direction < 0) // left
    {
        chunks_map.erase(std::prev(chunks_map.end()));
        new_pos = chunks_map.begin()->second->chunkpos - 1;
    }
    else if (direction > 0) // right
    {
        delete chunks_map.begin()->second;
        chunks_map.erase(chunks_map.begin());
        new_pos = std::prev(chunks_map.end())->second->chunkpos + 1;
    }
    chunks_map[new_pos] = new Chunk(new_pos, gen_function);
}

bool Map::is_loaded(int pos)
{
    return chunks_map.count(pos);
}

void Map::update()
{
    for (auto item : chunks_map)
    {
        item.second->update();
    }
}

void Map::draw(SDL_Renderer *renderer, const SDL_Rect *rect)
{
    for (auto item : chunks_map)
    {
        item.second->draw(renderer, rect, blocks_textures);
    }
}

collisions_list Map::get_collisions(const SDL_Rect *rect)
{
    collisions_list rects;
    SDL_Rect block_rect = {0, 0, TILE_SIZE, TILE_SIZE};
    int chunkpos = rect->x / CHUNK_WIDTH / TILE_SIZE;

    if (!is_loaded(chunkpos))
    {
        return rects;
    }

    block_t block;
    int start_x = chunkpos * TILE_SIZE * CHUNK_WIDTH;
    auto blocks = chunks_map[chunkpos]->blocks;

    for (int y = 0; y < CHUNK_HEIGHT; y++)
    {
        for (int x = 0; x < CHUNK_WIDTH; x++)
        {
            block = blocks[y][x];
            if (block == 0) // is not solid
            {
                continue;
            }
            block_rect.x = x * TILE_SIZE + start_x;
            block_rect.y = y * TILE_SIZE;
            if (colliderect(rect, &block_rect))
            {
                rects.push_back({block_rect.x, block_rect.y, block_rect.w, block_rect.h, block});
            }
        }
    }
    return rects;
}

void Map::dealloc_all()
{
    for (auto item : chunks_map)
    {
        delete item.second;
    }
}

bool colliderect(const SDL_Rect *A, const SDL_Rect *B)
{
    return (
        A->x + A->w > B->x &&
        A->x < B->x + B->w &&
        A->y + A->h > B->y &&
        A->y < B->y + B->h);
}
