
#include "player.hpp"

Player::Player(int x, int y, Map *game_map)
{
    this->rect.x = x;
    this->rect.y = y;
    this->rect.w = 10;
    this->rect.h = 20;
    this->game_map = game_map;
    this->flags |= ENTITY_PLAYER;
    this->flags |= ENTITY_SOLID;
    this->last_chunk = x / CHUNK_WIDTH / TILE_SIZE;
    // this->hide();
}

void Player::update()
{
    SolidEntity::update();
    int current_chunk = rect.x / CHUNK_WIDTH / TILE_SIZE;
    if (last_chunk != current_chunk)
    {
        if (last_chunk < current_chunk)
            game_map->shift_chunks(1);
        else
            game_map->shift_chunks(-1);
        last_chunk = current_chunk;
    }
}
