
#include "entity.hpp"
#include "logging.hpp"

Entity::Entity(int x, int y, SDL_Texture *image, Map *game_map)
{
    reset();
    this->rect.x = x;
    this->rect.y = y;
    this->image = image;
    this->game_map = game_map;
}

Entity::Entity()
{
    reset();
}

void Entity::reset()
{
    this->visible = true;
    this->persistent = false;
    this->interactable = false;
    this->triggerable = false;
    this->trigger_dist = -1;
    this->flags = 0;
    this->image = NULL;
    this->auto_mv = {0, 0};
    this->finished = false;
    this->game_map = NULL;
    this->rect = {0, 0, 0, 0};
    logs::info("entity allocated");
}

Entity::~Entity()
{
    // dealloc animations
    logs::info("entity deallocated");
}

bool Entity::can_interact()
{
    return this->interactable;
}

// int Entity::current_chunk()
// {
//     return this->rect.x / CHUNK_WIDTH;
// }

void Entity::show()
{
    this->visible = true;
}

void Entity::hide()
{
    this->visible = false;
}

bool Entity::is_shown()
{
    return this->visible;
}

void Entity::draw(SDL_Renderer *renderer, const SDL_Rect *addpos)
{
    if (!this->visible)
        return;
    SDL_Rect dst = {this->rect.x + addpos->x, this->rect.y + addpos->y, this->rect.w, this->rect.h};
    // printf("%d,%d,%d,%d\n", dst.x, dst.y, dst.w, dst.h);
    SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
    SDL_RenderFillRect(renderer, &dst);
}

void Entity::update()
{
}

void Entity::kill()
{
    this->finished = true;
}

void SolidEntity::move(struct movment movment)
{
    collisions = 0;

    rect.x += movment.addx;
    for (block_data block : game_map->get_collisions(&rect))
    {
        if (movment.addx > 0) // right
        {
            rect.x = block.x - rect.w;
            collisions |= COLLIDE_RIGHT;
        }
        else if (movment.addx < 0) // left
        {
            rect.x = block.x + block.w;
            collisions |= COLLIDE_LEFT;
        }
    }

    rect.y += movment.addy;
    for (block_data block : game_map->get_collisions(&rect))
    {
        if (movment.addy > 0) // bottom
        {
            rect.y = block.y - rect.h;
            collisions |= COLLIDE_DOWN;
        }
        else if (movment.addy < 0) // top
        {
            rect.y = block.y + block.h;
            collisions |= COLLIDE_UP;
        }
    }
}

void SolidEntity::jump(float force)
{
    // if not collide down, then return
    y_momentum = -force;
}

void SolidEntity::update()
{
    move({auto_mv.addx, auto_mv.addy + (int)y_momentum});
    if (collisions & COLLIDE_DOWN || collisions & COLLIDE_UP)
        y_momentum = 0;
    else
        y_momentum += GRAVITY_ADDPOS;
}
