#!/usr/bin/sh

mkdir -p build/

for file in src/* ; do
    outfile=$(basename $file)
    g++ -c -Wall -Iinclude $file -o build/${outfile%.*}.o &
done

g++ -c -Wall -Iinclude main.cpp -o build/main.o &
wait
g++ -o nuke build/* -lSDL2 -lSDL2_image
