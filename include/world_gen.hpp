
#ifndef _WORLD_GEN_HPP_
#define _WORLD_GEN_HPP_

#include "map.hpp"

// namespace world // for each world
// {
//     float gravity;
//      - gen function
//      - ...
// };

block_t genblock_earth(blockpos_t x, blockpos_t y);

#endif
