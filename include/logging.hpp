
#ifndef _LOGGING_HPP_
#define _LOGGING_HPP_

namespace logs
{
    void info(const char *message);
    void warn(const char *message);
    void error(const char *message);
};

#endif