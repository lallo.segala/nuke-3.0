
#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include <SDL2/SDL_rect.h>

class Camera
{
private:
    double addx;
    double addy;
    float zoom;
    SDL_Rect *target;
    float speed;
    int screen_width, screen_height;

public:
    Camera(float speed, int screen_width, int screen_height);
    int set_follow_target(SDL_Rect *target);
    void update();
    void get_addpos(SDL_Rect *ptr);
    int detach();
    void shake(int force, int duration);
    void set_screen_size(int w, int h);
};

#endif
