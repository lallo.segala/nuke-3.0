
#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "entity.hpp"

class Player : public SolidEntity
{
private:
    int last_chunk;

public:
    Player(int x, int y, Map *game_map);
    void update();
};

#endif