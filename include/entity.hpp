
#ifndef _ENTITY_HPP_
#define _ENTITY_HPP_

#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>

#include "map.hpp"

#define COLLIDE_UP 1
#define COLLIDE_DOWN 2
#define COLLIDE_LEFT 4
#define COLLIDE_RIGHT 8

#define GRAVITY_ADDPOS 0.15

#define ENTITY_SOLID 1
#define ENTITY_PLAYER 2

struct movment
{
    int addx, addy;
};

class Entity
{
private:
    bool visible;

protected:
    bool persistent;   // the entity will be saved into a file, when the chunk is unloaded (clound will not)
    bool interactable; // appare un buttone 'interact' quando si è vicini oppure premere I o X da controller
    bool triggerable;  // same as interactable but without button (e.g. a trap)
    int trigger_dist;  // TODO: max(deltaX, deltaY) or euclidian distance
    uint32_t flags;
    SDL_Texture *image; // replace with animations
    bool finished;      // entity will be be removed from ENTITY LIST
    Map *game_map;

    void reset();

public:
    struct movment auto_mv;
    SDL_Rect rect;

    Entity(int x, int y, SDL_Texture *image, Map *game_map);
    Entity();
    ~Entity();

    bool can_interact();
    // int current_chunk();

    void show();
    void hide();
    bool is_shown();

    void draw(SDL_Renderer *renderer, const SDL_Rect *addpos);
    void update();
    void kill();

    void interact(Entity *other);
    void trigger(Entity *other);

    /* 'data' with len 'size' will saved to disk */
    void getstate(int *id, int *size, void **data);

    /* called if entity is loaded from disk */
    void setstate(int id, int size, const void *data);

    // void leave(); -> called before saving entity
    // void getstate(void *ptr, int *size); -> data to save into file
};

class SolidEntity : public Entity
{
protected:
    float y_momentum;
    uint8_t collisions; // e.g. COLLIDE_UP | COLLIDE_LEFT
public:
    void move(struct movment addpos);
    void update();
    void jump(float jump_force);
};

#endif
