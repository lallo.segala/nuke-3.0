
#ifndef _IMAGES_HPP_
#define _IMAGES_HPP_

// #include "animation.hpp"
#include <SDL2/SDL_render.h>

SDL_Texture **load_blocks_images(SDL_Renderer *renderer, const char *path, int size);

#endif