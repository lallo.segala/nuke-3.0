
#ifndef _ANIMATION_HPP_
#define _ANIMATION_HPP_

#include <SDL2/SDL_stdinc.h> // Uint64
#include <SDL2/SDL_render.h> // SDL_Texture, SDL_Renderer, SDL_RendererFlip
#include <SDL2/SDL_rect.h>   // SDL_Rect

#define ANIMATION_IDLE
#define ANIMATION_WALK
#define ANIMATION_RUN
#define ANIMATION_JUMP
#define ANIMATION_TRIGGER
#define ANIMATION_INTERACT_BEFORE
#define ANIMATION_INTERACT_IDLE
#define ANIMATION_INTERACT_AFTER
#define ANIMATION_DIE
#define ANIMATION_RESPAWN

class Animation
{
private:
    bool stopped;
    bool finished;
    int index;
    Uint64 last_update;

public:
    unsigned int delay;
    int num_frames;
    SDL_Texture **frames;
    SDL_RendererFlip flipped;
    bool loop;

    Animation();
    ~Animation();

    bool set_loop(bool loop);

    bool pause();
    bool unpause();
    void finish();
    void update();
    void draw(SDL_Renderer *renderer, SDL_Rect *addpos);
};

#endif
