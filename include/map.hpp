
#ifndef _MAP_HPP_
#define _MAP_HPP_

#include <map>
#include <vector>

#include <stdint.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_rect.h>

#include "logging.hpp"

// #include "animation.hpp"

#define CHUNK_WIDTH 16
#define CHUNK_HEIGHT 64

#define TILE_SIZE 16

#define BLOCK_IO_OK 0
#define BLOCK_IO_INVALID -1
#define BLOCK_IO_UNLOADED 1
#define BLOCK_IO_NOTFOUND -2

typedef int64_t blockpos_t;
typedef uint16_t rel_blockpos_t;
typedef uint16_t block_t;
typedef block_t (*gen_function_t)(blockpos_t, blockpos_t);

struct block_data
{
    int x, y, w, h;
    block_t id;
    // HERE: add block info
};

typedef std::vector<struct block_data> collisions_list;

struct overwritten_block
{
    rel_blockpos_t x, y;
    block_t block_id;
};

typedef std::vector<struct overwritten_block> blocks_list;

class Chunk
{
    friend class Map;

private:
    block_t blocks[CHUNK_HEIGHT][CHUNK_WIDTH];
    int chunkpos;

public:
    Chunk(int chunkpos, gen_function_t gen_function);
    ~Chunk();
    void update();
    void draw(SDL_Renderer *renderer, const SDL_Rect *rect, SDL_Texture **blocks_textures);
};

class Map
{
private:
    int render_dist;
    gen_function_t gen_function;
    std::map<int, Chunk *> chunks_map;
    std::map<int, blocks_list> overwritten_blocks;
    Chunk load_chunk(int pos, gen_function_t gen_function);
    SDL_Texture **blocks_textures;

public:
    /* constructor */
    Map(int render_distance, gen_function_t gen_function, SDL_Texture **blocks_textures);

    /* load chunks based on 'rect' */
    void load_chunks(SDL_Rect *rect);

    /* unpack an absolute position into chunkpos and blockpos */
    void unpack_position(blockpos_t x, int *chunkpos, rel_blockpos_t *blockpos);

    /* replace the block at (x, y) with `block' */
    int setblock(blockpos_t x, blockpos_t y, block_t block);

    /* get the block at (x, y) */
    int getblock(blockpos_t x, blockpos_t y, block_t *block);

    /* update the `overwritten_blocks' map */
    void setblock_ovr(int chunkpos, rel_blockpos_t x, rel_blockpos_t y, block_t block);

    /* shift chunks in one direction */
    void shift_chunks(int direction);

    /* test if chunk is loaded */
    bool is_loaded(int pos);

    void update();
    void draw(SDL_Renderer *renderer, const SDL_Rect *rect);

    collisions_list get_collisions(const SDL_Rect *rect);

    void dealloc_all();
};

/* test if rect `A' collides with rect `B' */
bool colliderect(const SDL_Rect *A, const SDL_Rect *B);

#endif
